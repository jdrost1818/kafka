package gitlab.jdrost1818.kafka.producers;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class MessageProducer extends KafkaProducer<String, String>{

    private static Properties props = new Properties();

    static {
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "http://localhost:9092");
        props.put("key.serializer", KafkaAvroSerializer.class);
        props.put("value.serializer", KafkaAvroSerializer.class);
        props.put("schema.registry.url", "http://localhost:8081");
    }

    public MessageProducer() {
        super(props);
    }
}
