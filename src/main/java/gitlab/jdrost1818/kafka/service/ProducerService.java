package gitlab.jdrost1818.kafka.service;

import gitlab.jdrost1818.kafka.producers.MessageProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class ProducerService {

    private final MessageProducer messageProducer;

    @Autowired
    public ProducerService(MessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void pushToTopic(String message) {
        this.messageProducer.send(new ProducerRecord<String, String>("TextLinesTopic", message));
    }
}
