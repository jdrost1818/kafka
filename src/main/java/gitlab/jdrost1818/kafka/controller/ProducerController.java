package gitlab.jdrost1818.kafka.controller;

import gitlab.jdrost1818.kafka.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {

    private final ProducerService producerService;

    @Autowired
    public ProducerController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @RequestMapping("/message")
    public String message(@RequestParam(value="message", defaultValue = "No message sent") String message) {
        this.producerService.printMessage(message);
        this.producerService.pushToTopic(message);
        return "This is mapped right.";
    }
}
